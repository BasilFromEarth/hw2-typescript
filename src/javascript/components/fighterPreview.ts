import { createElement } from '../helpers/domHelper';
import { fighters } from '../helpers/mockData';


export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  // todo: show fighter info (image, name, health, etc.)

  const image = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: fighter.source },
  });
  const name = createElement({
    tagName: 'h1',
    className: 'arena___fighter-name'
  });
  name.append(fighter.name)
  const health = createElement({
    tagName: 'div',
    className: 'arena___fighter-name'
  });
  health.append(`Health: ${fighter.health}`);
  const attack = createElement({
    tagName: 'div',
    className: 'arena___fighter-name'
  });
  attack.append(`Attack: ${fighter.attack}`);
  const defense = createElement({
    tagName: 'div',
    className: 'arena___fighter-name'
  });
  defense.append(`Defense: ${fighter.defense}`);
  fighterElement.append(name, health, attack, defense, image);
  return fighterElement;
}

export function createFighterImage(fighter: object) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

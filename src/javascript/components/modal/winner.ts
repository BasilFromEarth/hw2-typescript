import { showModal } from './modal';
import App from '../../app';

export function showWinnerModal(fighter: {
  _id: string,
  name: string,
  health: number, 
  attack: number, 
  defense: number,
  source: string
}) {
  // call showModal function 
  console.log(fighter.name);
  showModal({title: 'Winner is', bodyElement: fighter.name});
}

import { controls } from '../../constants/controls';


export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    let h1 = firstFighter.health;
    let def1 = h1;
    let h2 = secondFighter.health;
    let def2 = h2;
    const h1Element = document.getElementById('left-fighter-indicator');
    const h2Element = document.getElementById('right-fighter-indicator');
    document.addEventListener("keydown", (event) => {
      if (event.defaultPrevented) {
        return; // Do nothing if the event was already processed
      }
      console.log(event.key)
      console.log(event)

      switch (event.key) {
        case 'a': case 'A':   

          h2 -= getHitPower(firstFighter);
          h2Element.style = `width: ${h2 * 100 / def2}%`;
          break;
        case controls.PlayerOneBlock:
          // Do something for "up arrow" key press.
          break;
        case 'j': case 'J':
          h1 -= getHitPower(secondFighter);
          h1Element.style = `width: ${h1 * 100 / def1}%`;
          break;
        case controls.PlayerTwoBlock:
          // Do something for "left arrow" key press.
          break;
          // Do something for "right arrow" key press.
        case controls.PlayerOneCriticalHitCombination:
          // Do something for "left arrow" key press.
          break;
        case controls.PlayerTwoCriticalHitCombination:

          break;
        default:
          return; // Quit when this doesn't handle the key event.
      }
      event.preventDefault();
      // resolve the promise with the winner when fight is over
      if(h1 < 0 || h2 < 0){
        resolve(h1 > h2 ? firstFighter : secondFighter);
      }
    }, true);  
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return (damage > 0 ? damage : 0);
}

export function getHitPower(fighter) {
  return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter) {
  return fighter.defense * (Math.random() + 1);
}

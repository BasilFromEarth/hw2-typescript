export function createElement(_a) {
    var _b;
    var tagName = _a.tagName, className = _a.className, _c = _a.attributes, attributes = _c === void 0 ? {} : _c;
    var element = document.createElement(tagName);
    if (className) {
        var classNames = className.split(' ').filter(Boolean);
        (_b = element.classList).add.apply(_b, classNames);
    }
    Object.keys(attributes).forEach(function (key) { return element.setAttribute(key, attributes[key]); });
    return element;
}

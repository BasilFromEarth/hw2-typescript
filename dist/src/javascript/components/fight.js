var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { controls } from '../../constants/controls';
export function fight(firstFighter, secondFighter) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, new Promise(function (resolve) {
                    var h1 = firstFighter.health;
                    var def1 = h1;
                    var h2 = secondFighter.health;
                    var def2 = h2;
                    var h1Element = document.getElementById('left-fighter-indicator');
                    var h2Element = document.getElementById('right-fighter-indicator');
                    document.addEventListener("keydown", function (event) {
                        if (event.defaultPrevented) {
                            return; // Do nothing if the event was already processed
                        }
                        console.log(event.key);
                        console.log(event);
                        switch (event.key) {
                            case 'a':
                            case 'A':
                                h2 -= getHitPower(firstFighter);
                                h2Element.style = "width: " + h2 * 100 / def2 + "%";
                                break;
                            case controls.PlayerOneBlock:
                                // Do something for "up arrow" key press.
                                break;
                            case 'j':
                            case 'J':
                                h1 -= getHitPower(secondFighter);
                                h1Element.style = "width: " + h1 * 100 / def1 + "%";
                                break;
                            case controls.PlayerTwoBlock:
                                // Do something for "left arrow" key press.
                                break;
                            // Do something for "right arrow" key press.
                            case controls.PlayerOneCriticalHitCombination:
                                // Do something for "left arrow" key press.
                                break;
                            case controls.PlayerTwoCriticalHitCombination:
                                break;
                            default:
                                return; // Quit when this doesn't handle the key event.
                        }
                        event.preventDefault();
                        // resolve the promise with the winner when fight is over
                        if (h1 < 0 || h2 < 0) {
                            resolve(h1 > h2 ? firstFighter : secondFighter);
                        }
                    }, true);
                })];
        });
    });
}
export function getDamage(attacker, defender) {
    // return damage
    var damage = getHitPower(attacker) - getBlockPower(defender);
    return (damage > 0 ? damage : 0);
}
export function getHitPower(fighter) {
    return fighter.attack * (Math.random() + 1);
}
export function getBlockPower(fighter) {
    return fighter.defense * (Math.random() + 1);
}

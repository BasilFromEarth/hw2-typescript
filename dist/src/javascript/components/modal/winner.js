import { showModal } from './modal';
export function showWinnerModal(fighter) {
    // call showModal function 
    console.log(fighter.name);
    showModal({ title: 'Winner is', bodyElement: fighter.name });
}

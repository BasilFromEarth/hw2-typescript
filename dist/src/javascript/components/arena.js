import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';
export function renderArena(selectedFighters) {
    var root = document.getElementById('root');
    var arena = createArena(selectedFighters);
    root.innerHTML = '';
    root.append(arena);
    // - start the fight
    // - when fight is finished show winner
    var playerOne = selectedFighters[0], playerTwo = selectedFighters[1];
    fight(playerOne, playerTwo)
        .then(function (winner) { showWinnerModal(winner); });
}
function createArena(selectedFighters) {
    var arena = createElement({ tagName: 'div', className: 'arena___root' });
    var healthIndicators = createHealthIndicators.apply(void 0, selectedFighters);
    var fighters = createFighters.apply(void 0, selectedFighters);
    arena.append(healthIndicators, fighters);
    return arena;
}
function createHealthIndicators(leftFighter, rightFighter) {
    var healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
    var versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
    var leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
    var rightFighterIndicator = createHealthIndicator(rightFighter, 'right');
    healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
    return healthIndicators;
}
function createHealthIndicator(fighter, position) {
    var name = fighter.name;
    var container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
    var fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
    var indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
    var bar = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: position + "-fighter-indicator" } });
    fighterName.innerText = name;
    indicator.append(bar);
    container.append(fighterName, indicator);
    return container;
}
function createFighters(firstFighter, secondFighter) {
    var battleField = createElement({ tagName: 'div', className: "arena___battlefield" });
    var firstFighterElement = createFighter(firstFighter, 'left');
    var secondFighterElement = createFighter(secondFighter, 'right');
    battleField.append(firstFighterElement, secondFighterElement);
    return battleField;
}
function createFighter(fighter, position) {
    var imgElement = createFighterImage(fighter);
    var positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
    var fighterElement = createElement({
        tagName: 'div',
        className: "arena___fighter " + positionClassName,
    });
    fighterElement.append(imgElement);
    return fighterElement;
}

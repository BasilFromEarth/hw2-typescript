import { createElement } from '../helpers/domHelper';
import { createFightersSelector } from './fighterSelector';
export function createFighters(fighters) {
    var selectFighter = createFightersSelector();
    var container = createElement({ tagName: 'div', className: 'fighters___root' });
    var preview = createElement({ tagName: 'div', className: 'preview-container___root' });
    var fightersList = createElement({ tagName: 'div', className: 'fighters___list' });
    var fighterElements = fighters.map(function (fighter) { return createFighter(fighter, selectFighter); });
    fightersList.append.apply(fightersList, fighterElements);
    container.append(preview, fightersList);
    return container;
}
function createFighter(fighter, selectFighter) {
    var fighterElement = createElement({ tagName: 'div', className: 'fighters___fighter' });
    var imageElement = createImage(fighter);
    var onClick = function (event) { return selectFighter(event, fighter._id); };
    fighterElement.append(imageElement);
    fighterElement.addEventListener('click', onClick, false);
    return fighterElement;
}
function createImage(fighter) {
    var source = fighter.source, name = fighter.name;
    var attributes = {
        src: source,
        title: name,
        alt: name,
    };
    var imgElement = createElement({
        tagName: 'img',
        className: 'fighter___fighter-image',
        attributes: attributes
    });
    return imgElement;
}

import { createElement } from '../helpers/domHelper';
export function createFighterPreview(fighter, position) {
    var positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
    var fighterElement = createElement({
        tagName: 'div',
        className: "fighter-preview___root " + positionClassName,
    });
    // todo: show fighter info (image, name, health, etc.)
    var image = createElement({
        tagName: 'img',
        className: 'preview-container___versus-img',
        attributes: { src: fighter.source },
    });
    var name = createElement({
        tagName: 'h1',
        className: 'arena___fighter-name'
    });
    name.append(fighter.name);
    var health = createElement({
        tagName: 'div',
        className: 'arena___fighter-name'
    });
    health.append("Health: " + fighter.health);
    var attack = createElement({
        tagName: 'div',
        className: 'arena___fighter-name'
    });
    attack.append("Attack: " + fighter.attack);
    var defense = createElement({
        tagName: 'div',
        className: 'arena___fighter-name'
    });
    defense.append("Defense: " + fighter.defense);
    fighterElement.append(name, health, attack, defense, image);
    return fighterElement;
}
export function createFighterImage(fighter) {
    var source = fighter.source, name = fighter.name;
    var attributes = {
        src: source,
        title: name,
        alt: name
    };
    var imgElement = createElement({
        tagName: 'img',
        className: 'fighter-preview___img',
        attributes: attributes,
    });
    return imgElement;
}
